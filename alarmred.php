<?php
    /* The following is a sample code snippet. Please note that the API key and
      shared secret are bogus and will not work.
      
      I'm using OAuthSimple as the library to perform the signatures. It
      currently is available for PHP and Javascript, but I'd REALLY appreciate
      it if folks could help flesh out versions for Python, .net, and other
      languages.
    */
	
    include ('./OAuthSimple.php');
        
    /* Remember, these are bogus. Swap them for the API key and Shared Secret
      you got when you registered your Application at
      http://developer.netflix.com
    */
	$user = 'root';
	$pass = 'root';	
	$authURL = "$_POST[oauth_token]";
	//$authSec = 'RyBYKqH5M59C';//"$_GET[oauth_token_secret]";
    $apiKey = 'fbhx9wfjk6shf77ttshngvkk';
    $sharedSecret = 'gv6GfFtKfj';
	$accessToken = 'mnug8dkn76ph6hm76wx4x8rq';
	$tokenSecret = 'VFuc93Kvzz8Y';
	$appName = 'Netflix+Alarm+Clock';
	
	try {
	$dbh = new PDO('mysql:host=localhost;dbname=nfxalarmclock', $user, $pass);
	
	$stmt = $dbh->prepare("SELECT secret FROM  `oauth` WHERE token = ?");
	$stmt->execute(array($authURL));
	$secret = $stmt->fetch();
	$authSec = $secret['secret'];		
    } 
	catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
    }
	
    // Some sample argument values

    /* You can pass in arguments to OAuthSimple either as a string of URL
      characters or an array. (See the documentation for OAuthSimple for
      details. There's nothing magical going on here, just simple key=>value
      pairs. */
    $arguments = Array(
        'output'=>'json'
    );
    // this is the URL path (note the lack of arguments.)
    $path = "http://api-public.netflix.com/oauth/access_token";

    // Create the Signature object.
    $oauth = new OAuthSimple();
    $signed = $oauth->sign(Array('path'=>$path,
                    'parameters'=>$arguments,
                    'signatures'=> Array('consumer_key'=>$apiKey,
                                        'shared_secret'=>$sharedSecret,
                                        'access_token'=>$authURL,
                                        'access_secret'=>$authSec
                                        
                                        )));
    
    // Now go fetch the data.
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,$signed['signed_url']);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curl,100,2);
    $buffer = curl_exec($curl);
    if (curl_errno($curl))
    {
        die ("An error occurred:".curl_error());
    }
    $result = json_decode($buffer,true);
	
	try {
	$stmt = $dbh->prepare("INSERT INTO users (user_id, oauth_token, oauth_secret) 
		VALUES (:user_id, :oauth_token, :oauth_secret)");
	$stmt->bindParam(':user_id', $result['user_id']);
	$stmt->bindParam(':oauth_token', $result['oauth_token']);
	$stmt->bindParam(':oauth_secret', $result['oauth_token_secret']);
	$stmt->execute();
	$dbh = null;
    } catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
    }
	setcookie('nf_user_id', $result['user_id'], time()+60*60*24*30);	
	//echo $buffer;
	
    
	/*
    $newpath = 'http://api-public.netflix.com/users/' . $result['user_id'];
    
    
    //New Signature Objected
    $newoauth = new OAuthSimple();
    $newsigned = $newoauth->sign(Array('path'=>$newpath,
    						'parameters'=>$arguments,
    						'signatures'=>Array('consumer_key'=>$apiKey,
                                        'shared_secret'=>$sharedSecret,
                                        'access_token'=>$result['oauth_token'],
                                        'access_secret'=>$result['oauth_token_secret']
                                        )));
    //fetching data
    $newcurl = curl_init();
    curl_setopt($newcurl,CURLOPT_URL,$newsigned['signed_url']);
    curl_setopt($newcurl,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($newcurl,100,2);
    $newbuffer = curl_exec($newcurl);
    if (curl_errno($newcurl))
    {
        die ("An error occurred:".curl_error());
    }
    $newresult = json_decode($newbuffer,true);                               
	*/	
?>
